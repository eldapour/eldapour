<h1 align="center"><b>Hi , I'm Abdalluh Mahmoud </b><img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="35"></h1>

<p align="center">
  <a href="https://github.com/DenverCoder1/readme-typing-svg"><img src="https://readme-typing-svg.herokuapp.com?font=Time+New+Roman&color=cyan&size=25&center=true&vCenter=true&width=600&height=100&lines=ABDALLUH+MAHMOUD+(Eldapour)..🤖;+;Back-End+Developer;Python+Developer;Kali+Linx+Researcher;Active+Learner/Researcher,;Love+to+learn+new+Language..<3;++;"></a>
</p>

<br>

## <picture><img src = "https://github.com/0xAbdulKhalid/0xAbdulKhalid/raw/main/assets/mdImages/about_me.gif" width = 50px></picture> **About Me**

<picture> <img align="right" src="https://github.com/0xAbdulKhalid/0xAbdulKhalid/raw/main/assets/mdImages/Right_Side.gif" width = 250px></picture>

<br>

- Back-end developer
- PHP & python
- Currently learning Ai & Machine Learning by Self
- last works [link](https://solarvalleypv.com)
- I’m currently open for an Intern or a new job opportunity, this is [my resume](https://read.cv/join/eldapour)
- [MY WORKS](https://read.cv/join/eldapour)

<br><br>

<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif"><br><br>

## <img src="https://media2.giphy.com/media/QssGEmpkyEOhBCb7e1/giphy.gif?cid=ecf05e47a0n3gi1bfqntqmob8g9aid1oyj2wr3ds3mg700bl&rid=giphy.gif" width ="25"><b> Skills</b>

<br>

<p align="center">

- **Languages**:

  ![PHP](https://img.shields.io/badge/PHP%20-blue?style=for-the-badge&logo=php&logoColor=white)
  ![api](https://img.shields.io/badge/api%20-ff6c37?style=for-the-badge&logo=postman&logoColor=white)
  ![Laravel](https://img.shields.io/badge/Laravel-red?style=for-the-badge&logo=laravel&logoColor=white)
  ![jquery](https://img.shields.io/badge/JQUERY%20-%23F7DF1E.svg?style=for-the-badge&logo=jquery&logoColor=black)
  ![Python](https://img.shields.io/badge/Python%20-%2314354C.svg?style=for-the-badge&logo=python&logoColor=white)
  ![HTML5](https://img.shields.io/badge/HTML5%20-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
  ![CSS3](https://img.shields.io/badge/CSS%20-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white)
  ![vue.js](https://img.shields.io/badge/vue.js%20-42b883?style=for-the-badge&logo=vue.js&logoColor=white)
  ![bootstrap](https://img.shields.io/badge/bootstrap%20-6f2bf6?style=for-the-badge&logo=bootstrap&logoColor=white)
  ![JavaScript](https://img.shields.io/badge/JavaScript%20-%23F7DF1E.svg?style=for-the-badge&logo=javascript&logoColor=black)
  ![Ajax](https://img.shields.io/badge/AJAX%20-%23F7DF1E.svg?style=for-the-badge&logo=javascript&logoColor=black)
  ![livewire](https://img.shields.io/badge/livewire%20-FB70A9?style=for-the-badge&logo=livewire&logoColor=white)

<br>

- **Cloud Hosting**:

  ![Github Pages](https://img.shields.io/badge/GitHub%20Pages-%23327FC7.svg?style=for-the-badge&logo=github&logoColor=white)
  ![Hostinger](https://img.shields.io/badge/Hostinger%20-6a3eec?style=for-the-badge&logo=&logoColor=white)
  ![godaddy](https://img.shields.io/badge/godaddy%20-00838c?style=for-the-badge&logo=godaddy&logoColor=white)
  ![bluehost](https://img.shields.io/badge/bluehost%20-196bde?style=for-the-badge&logo=&logoColor=white)
  ![dreamhost](https://img.shields.io/badge/dreamhost%20-007bff?style=for-the-badge&logo=r&logoColor=white)

<br>

- **Softwares and Tools**:

  ![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
  ![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)
  ![Google](https://img.shields.io/badge/google-%234285F4.svg?style=for-the-badge&logo=google&logoColor=white)
  ![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
  ![jetbrains](https://img.shields.io/badge/jetbrains-black?style=for-the-badge&logo=jetbrains&logoColor=white)
  ![phpstorm](https://img.shields.io/badge/phpstorm-6b57ff?style=for-the-badge&logo=phpstorm&logoColor=white)
  ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)
  ![ubuntu](https://img.shields.io/badge/ubuntu-e95420?style=for-the-badge&logo=ubuntu&logoColor=black)

<br>

- **Extras**:

  ![Terminal](https://img.shields.io/badge/Terminal-%23054020?style=for-the-badge&logo=gnu-bash&logoColor=white)
  ![powershell](https://img.shields.io/badge/powershell-blue?style=for-the-badge&logo=powershell&logoColor=white)
  ![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white)
  ![Git bash](https://img.shields.io/badge/git%20bash-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)

</p>

<br>
<br>

<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">

<br>

## <img src="https://media.giphy.com/media/iY8CRBdQXODJSCERIr/giphy.gif" width="35"><b> Github Stats </b>

<br>

<div align="center">

<a href="https://github.com/eldapour/">
  <img src="https://github-readme-stats.vercel.app/api?username=eldapour&include_all_commits=true&count_private=true&show_icons=true&line_height=20&title_color=7A7ADB&icon_color=2234AE&text_color=D3D3D3&bg_color=0,000000,130F40" width="450"/>
  <img src="https://github-readme-stats.vercel.app/api/top-langs?username=eldapour&show_icons=true&locale=en&layout=compact&line_height=20&title_color=7A7ADB&icon_color=2234AE&text_color=D3D3D3&bg_color=0,000000,130F40" width="450"  alt="eldapour"/>

</a>
</div>

<br>
<br>
<br>

<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">

<br>
<br>

## <b> Let's Connect..!</b><img src="https://github.com/0xAbdulKhalid/0xAbdulKhalid/raw/main/assets/mdImages/handshake.gif" width ="80">

<br>
<div align='left'>

<ul>

<li>
<a href="https://www.linkedin.com/in/el-dapour/" target="_blank">
<img src="https://img.shields.io/badge/linkedin:  abdallah mahmoud-%2300acee.svg?color=405DE6&style=for-the-badge&logo=linkedin&logoColor=white" alt=linkedin style="margin-bottom: 5px;"/>
</a>
</li>

<br>

<li>
<a href="https://twitter.com/el_dapour" target="_blank">
<img src="https://img.shields.io/badge/twitter:    abdallah mahmoud-%2300acee.svg?color=1DA1F2&style=for-the-badge&logo=twitter&logoColor=white" alt=twitter style="margin-bottom: 5px;"/>
</a>
</li>

<br>
 <li>
<a href="https://www.facebook.com/abdallah.mahmoud.1422003" target="_blank">
<img src="https://img.shields.io/badge/facebook:  abdallah mahmoud -3c5a99?style=for-the-badge&logo=facebook&logoColor=white" t=mail style="margin-bottom: 5px;" />
</a>

</li>

<br>

<li>
<a href="mailto:abdalluhmahmoud55@gmail.com" target="_blank">
<img src="https://img.shields.io/badge/gmail:  abdallah mahmoud 1-%23EA4335.svg?style=for-the-badge&logo=gmail&logoColor=white" t=mail style="margin-bottom: 5px;" />
</a>
</li>

<br>

<li>
<a href="mailto:abdallahmahmoud1422003@gmail.com" target="_blank">
<img src="https://img.shields.io/badge/gmail:  abdallah mahmoud 2-%23EA4335.svg?style=for-the-badge&logo=gmail&logoColor=white" t=mail style="margin-bottom: 5px;" />
</a>
</li>
</ul>
</div>

<!-- <br>
<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif"> -->
<br>
<br>
<!-- <br> -->
<!-- <br> -->


<!-- <div align='center'>

## <b> GOOD BYE ❤️👋 </b>

</div> -->
<!-- <br> -->
<!-- <br> -->
<!-- <br> -->


<img src="https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif">
<br>

<br>

  ![GitHub](https://img.shields.io/badge/Credit:-eldapour-greem?style=for-the-badge&logo=github&logoColor=white?url=https%3A%2F%2Fwww.github.com%2Feldapour)


Last Edited on: 14/02/2023
